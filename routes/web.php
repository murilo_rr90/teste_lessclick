<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('clients', 'ClientsController@index')->name('clients.index');
Route::get('clients/create/', 'ClientsController@create')->name('clients.create');
Route::get('clients/edit/{client}', 'ClientsController@edit')->name('clients.edit');
Route::get('clients/show/{client}', 'ClientsController@show')->name('clients.show');

Route::put('clients/update/{client}', 'ClientsController@update')->name('clients.update');
Route::post('clients/store/', 'ClientsController@store')->name('clients.store');
Route::get('clients/delete/{client}', 'ClientsController@delete')->name('clients.delete');
