<?php

use Illuminate\Http\Request;
Use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('clients', 'API\ClientsController@index')->name('api.clients');
Route::get('clients/{client}', 'API\ClientsController@show');
Route::post('clients', 'API\ClientsController@store');
Route::put('clients/{client}', 'API\ClientsController@update');
Route::delete('clients/{client}', 'API\ClientsController@delete');
