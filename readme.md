## LESSCLICK/ARENA - Teste Murilo

## Instalação

- git clone git@bitbucket.org:murilo_rr90/teste_murilo.git
- cd teste_murilo
- composer install
- cp .env.example .env (e editar acesso ao BD)
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan serve

Acessar http://localhost:8000 no navegador

## API - HTTP REQUESTS
Listar Clientes:
- GET http://localhost:8000/api/clients
- Parametros: page

Exibir Cliente:
- GET http://localhost:8000/api/clients/{id}

Criar Cliente:
- POST http://localhost:8000/api/clients
- Parametros: name, email, description

Editar Cliente:
- PUT http://localhost:8000/api/clients/{id}
- Parametros: name, email, description

Deletar Cliente:
- DELETE http://localhost:8000/api/clients/{id}

## Dependências

- Git
- PHP
- MySQL
- Composer

## Versões

- PHP     -> 7.1.17
- MySQL   -> 5.7.22

## Desenvovimento

Murilo R. Rocha