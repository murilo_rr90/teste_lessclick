@extends('layouts.app')
@section('model', 'Clientes')
@section('action', 'Listagem')

@section('content')
        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>ID</td>
                <td>Nome</td>
                <td>Email</td>
                <td style="width:22%">Ações</td>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>

                    <td >
                        <a class="btn btn-small btn-success" href="{{ route('clients.show', $item->id) }}">Exibir</a>
                        <a class="btn btn-small btn-info" href="{{ route('clients.edit', $item->id) }}">Editar</a>
                        <a class="btn btn-small btn-danger" href="{{ route('clients.delete', $item->id) }}">Deletar</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
@endsection